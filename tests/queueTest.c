/*******************************************************************************
 *    File: queueListTest.c
 *    Project: nachoLib
 *    Creation date: 13.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 21.03.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/
#include <stdbool.h>
#include <string.h>

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "Log.h"
#include "Queue.h"


/*******************************************************************************
                                    MAIN
*******************************************************************************/

int main (void)
{
    LOG_INFO("Test Queue running...")

    // Bool FIFO
    queue_t* q1 = queue_create(FIFO, Bool);
    bool bit1 = false;
    queue_push(q1, (void*)&bit1, sizeof(bool));
    bool bit2 = true;
    queue_push(q1, (void*)&bit2, sizeof(bool));


    bool r1 = queue_pop_bool(q1);
    bool r2 = queue_pop_bool(q1);

    if (r1 != bit1 || r2 != bit2)
    {
        LOG_ERROR("Test bool failed")
        return -1;
    }

    queue_destroy(q1);

    // Bool LIFO
    queue_t* qq1 = queue_create(LIFO, Bool);
    bool bbit1 = false;
    queue_push(qq1, (void*)&bbit1, sizeof(bool));
    bool bbit2 = true;
    queue_push(qq1, (void*)&bbit2, sizeof(bool));


    bool rr2 = queue_pop_bool(qq1);
    bool rr1 = queue_pop_bool(qq1);

    if (rr1 != bbit1 || rr2 != bbit2)
    {
        LOG_ERROR("Test bool failed")
        return -1;
    }

    queue_destroy(qq1);

    LOG_INFO("All tests passed for Queue")
    return 0;
}

/*******************************************************************************
                                END OF FILE
*******************************************************************************/