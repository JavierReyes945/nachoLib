/*******************************************************************************
 *    File: serverTest.c
 *    Project: nachoLib
 *    Creation date: 09.04.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 12.04.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/**
 * @file Server.c
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-04-09
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <inttypes.h>
#include <regex.h>

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "Server.h"
#include "Log.h"

/*******************************************************************************
                                    DEFINES
*******************************************************************************/

/**
 * @def BACKLOG
 * 
 * @brief Number of concurrent transactions that the server accepts
 */
#define BACKLOG         255

/**
 * @def IP_ADDR_REGEX
 * 
 * @brief Extended regular expression for matching a valid IP address
 */
#define IP_ADDR_REGEX   "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$"

/*******************************************************************************
                            PRIVATE VARIABLES
*******************************************************************************/

static bool stop_server = false;

/*******************************************************************************
                        PRIVATE FUNCTION PROTOTYPES
*******************************************************************************/

static void interrupt_handler (int sig);
static int  validate_ip       (const char* ip_str);

/*******************************************************************************
                        PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/

/**
 * @brief Function that will be called when the Ctrl+C (SIGINT) is received
 * 
 * @param sig Value of the INT type received from the Kernel
 */
static void interrupt_handler(int sig)
{
    // Ctrl+C
    if (sig == SIGINT)
    {
        LOG_INFO("Signal received...")
        stop_server = true;
    }
}

/**
 * @brief Uses POSIX regex to validate whether a given string is a valid IP address
 * 
 * @param ip_str Pointer to the string array to validate
 * @return int 0 when address is valid, -1 otherwise
 */
static int validate_ip(const char* ip_str)
{
    // Dont allow empty strings
    if (ip_str == NULL)
    {
        return -1;
    }

    // Generate the regex object
    regex_t regex;
    if (regcomp(&regex, IP_ADDR_REGEX, REG_EXTENDED) != 0)
    {
        LOG_ERROR("Regex compile error")
        exit(EXIT_FAILURE);
    }

    // Execute the comparisson with the argument
    int status = regexec(&regex, ip_str, 0, NULL, 0);
    if (status == REG_NOMATCH)
    {
        LOG_ERROR("Regex match not found: %s", ip_str)
        return -1;
    }
    else if (status == 0)
    {
        LOG_DEBUG("Regex match found: %s", ip_str)
        return 0;
    }
    else
    {
        LOG_ERROR("Regex other error: %s", ip_str)
        return -1;
    }

    // Release any regex memory
    regfree(&regex);
}

/*******************************************************************************
                                    MAIN
*******************************************************************************/

int main(int argc, char* argv[])
{
    socket_type_t sock_type = 0;
    char* ip_addr_str;
    uint16_t port_num;

    // CLI parameter count validation
    if (argc != 4)
    {
        LOG_ERROR("Missing parameter")
        LOG_INFO("Usage: serverTest (tcp|udp) IP_ADDR PORT")
        exit(EXIT_FAILURE);
    }

    // First parameter validation, either "tcp" or "udp"
    if (strcmp("tcp", argv[1]) == 0)
    {
        sock_type = STREAM;
    }
    else
    {
        if (strcmp("udp", argv[1]) == 0)
        {
            sock_type = DATAGRAM;
        }
        else
        {
            LOG_ERROR("Wrong parameter: %s", argv[1])
            LOG_INFO("Usage: serverTest (tcp|udp) IP_ADDR PORT")
            exit(EXIT_FAILURE);
        }
    }

    // Second parameter validation, must be a valid IP address
    if (validate_ip(argv[2]) == -1)
    {
        LOG_ERROR("Wrong parameter: %s", argv[2])
        LOG_INFO("Usage: serverTest (tcp|udp) IP_ADDR PORT")
        exit(EXIT_FAILURE);
    }
    else
    {
        ip_addr_str = argv[2];
    }

    // Third parameter validation, must be a number string
    for (size_t i = 0; i < strlen(argv[3]); i++)
    {
        if (isdigit(argv[3][i]) == 0)
        {
            LOG_ERROR("Wrong parameter: %s", argv[3])
            LOG_INFO("Usage: serverTest (tcp|udp) PORT")
            exit(EXIT_FAILURE);
        }
    }

    // Convert third parameter to integer in range [0-65535]
    if (sscanf(argv[3], "%" SCNu16, &port_num) != 1)
    {
        LOG_ERROR("Wrong parameter: %s", argv[2])
        LOG_INFO("Usage: serverTest (tcp|udp) PORT")
        exit(EXIT_FAILURE);
    }

    LOG_INFO("Test Server running...")

    // Capture Interrupt signal Ctrl+C
    signal(SIGINT, interrupt_handler);

    // Create TCP Server Object
    server_t* serverTest = server_create(IPV4, sock_type, ip_addr_str, port_num, BACKLOG);
    server_launch(serverTest);

    LOG_INFO("Running...")
    while (stop_server == false)
    {
        sleep(1);
    }

    // Stop server
    server_shutdown(serverTest);
    server_destroy(serverTest);

    return 0;
}

/*******************************************************************************
                                END OF FILE
*******************************************************************************/