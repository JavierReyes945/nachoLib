/*******************************************************************************
 *    File: nodeTest.c
 *    Project: nachoLib
 *    Creation date: 13.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 21.03.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "Log.h"
#include "Node.h"

/*******************************************************************************
                                    MAIN
*******************************************************************************/

int main (void)
{
    LOG_INFO("Test Node running...")
    bool test_ok = true;

    // Bool
    bool refbool1 = false;
    node_t* nbool1 = node_create((void*)&refbool1, Bool, sizeof(bool));

    bool refbool2 = true;
    node_t* nbool2 = node_create((void*)&refbool2, Bool, sizeof(bool));

    uint64_t bool1size = node_get_size(nbool1);
    uint64_t bool2size = node_get_size(nbool2);

    if (bool1size != sizeof(bool) || bool2size != sizeof(bool))
    {
        LOG_ERROR("Test bool failed")
        test_ok = false;
    }

    bool tbool1 = node_get_data_bool(nbool1);
    bool tbool2 = node_get_data_bool(nbool2);

    if (tbool1 != refbool1 || tbool2 != refbool2)
    {
        LOG_ERROR("Test bool failed")
        test_ok = false;
    }

    node_destroy(nbool1);
    node_destroy(nbool2);

    // Char
    char refchar1 = 'a';
    node_t* nchar1 = node_create((void*)&refchar1, Char, sizeof(char));

    char refchar2 = 'z';
    node_t* nchar2 = node_create((void*)&refchar2, Char, sizeof(char));

    uint64_t char1size = node_get_size(nchar1);
    uint64_t char2size = node_get_size(nchar2);

    if (char1size != sizeof(char) || char2size != sizeof(char))
    {
        LOG_ERROR("Test char failed")
        test_ok = false;
    }

    char tchar_1 = node_get_data_char(nchar1);
    char tchar_2 = node_get_data_char(nchar2);

    if (tchar_1 != refchar1 || tchar_2 != refchar2)
    {
        LOG_ERROR("Test char failed")
        test_ok = false;
    }

    node_destroy(nchar1);
    node_destroy(nchar2);

    // UInt8
    uint8_t uint8_1 = 8;
    node_t* n_uint8_1 = node_create((void*)&uint8_1, UInt8, sizeof(uint8_t));

    uint8_t uint8_2 = 208;
    node_t* n_uint8_2 = node_create((void*)&uint8_2, UInt8, sizeof(uint8_t));

    uint8_t r_uint8_1 = node_get_data_uint8(n_uint8_1);
    uint8_t r_uint8_2 = node_get_data_uint8(n_uint8_2);

    if (r_uint8_1 != uint8_1 || r_uint8_2 != uint8_2)
    {
        LOG_ERROR("Test uint8 failed")
        test_ok = false;
    }

    node_destroy(n_uint8_1);
    node_destroy(n_uint8_2);

    // Int8
    int8_t int8_1 = -8;
    node_t* n_int8_1 = node_create((void*)&int8_1, Int8, sizeof(int8_t));

    int8_t int8_2 = 208;
    node_t* n_int8_2 = node_create((void*)&int8_2, Int8, sizeof(int8_t));

    int8_t r_int8_1 = node_get_data_int8(n_int8_1);
    int8_t r_int8_2 = node_get_data_int8(n_int8_2);

    if (r_int8_1 != int8_1 || r_int8_2 != int8_2)
    {
        LOG_ERROR("Test int8 failed")
        test_ok = false;
    }

    node_destroy(n_int8_1);
    node_destroy(n_int8_2);

    // UInt16
    uint16_t uint16_1 = 1016;
    node_t* n_uint16_1 = node_create((void*)&uint16_1, UInt16, sizeof(uint16_t));

    uint16_t uint16_2 = 20016;
    node_t* n_uint16_2 = node_create((void*)&uint16_2, UInt16, sizeof(uint16_t));

    uint16_t r_uint16_1 = node_get_data_uint16(n_uint16_1);
    uint16_t r_uint16_2 = node_get_data_uint16(n_uint16_2);

    if (r_uint16_1 != uint16_1 || r_uint16_2 != uint16_2)
    {
        LOG_ERROR("Test uint16 failed")
        test_ok = false;
    }

    node_destroy(n_uint16_1);
    node_destroy(n_uint16_2);

    // Int16
    int16_t int16_1 = -1016;
    node_t* n_int16_1 = node_create((void*)&int16_1, Int16, sizeof(int16_t));

    int16_t int16_2 = 20016;
    node_t* n_int16_2 = node_create((void*)&int16_2, Int16, sizeof(int16_t));

    int16_t r_int16_1 = node_get_data_int16(n_int16_1);
    int16_t r_int16_2 = node_get_data_int16(n_int16_2);

    if (r_int16_1 != int16_1 || r_int16_2 != int16_2)
    {
        LOG_ERROR("Test int16 failed")
        test_ok = false;
    }

    node_destroy(n_int16_1);
    node_destroy(n_int16_2);

    // UInt32
    uint32_t uint32_1 = 100032;
    node_t* n_uint32_1 = node_create((void*)&uint32_1, UInt32, sizeof(uint32_t));

    uint32_t uint32_2 = 2000000032;
    node_t* n_uint32_2 = node_create((void*)&uint32_2, UInt32, sizeof(uint32_t));

    uint32_t r_uint32_1 = node_get_data_uint32(n_uint32_1);
    uint32_t r_uint32_2 = node_get_data_uint32(n_uint32_2);

    if (r_uint32_1 != uint32_1 || r_uint32_2 != uint32_2)
    {
        LOG_ERROR("Test uint32 failed")
        test_ok = false;
    }

    node_destroy(n_uint32_1);
    node_destroy(n_uint32_2);

    // Int32
    int32_t int32_1 = -100032;
    node_t* n_int32_1 = node_create((void*)&int32_1, Int32, sizeof(int32_t));

    int32_t int32_2 = 2000000032;
    node_t* n_int32_2 = node_create((void*)&int32_2, Int32, sizeof(int32_t));

    int32_t r_int32_1 = node_get_data_int32(n_int32_1);
    int32_t r_int32_2 = node_get_data_int32(n_int32_2);

    if (r_int32_1 != int32_1 || r_int32_2 != int32_2)
    {
        LOG_ERROR("Test int32 failed")
        test_ok = false;
    }

    node_destroy(n_int32_1);
    node_destroy(n_int32_2);

    // UInt64
    uint64_t uint64_1 = 1000000000064;
    node_t* n_uint64_1 = node_create((void*)&uint64_1, UInt64, sizeof(uint64_t));

    uint64_t uint64_2 = 2000000000000000064;
    node_t* n_uint64_2 = node_create((void*)&uint64_2, UInt64, sizeof(uint64_t));

    uint64_t r_uint64_1 = node_get_data_uint64(n_uint64_1);
    uint64_t r_uint64_2 = node_get_data_uint64(n_uint64_2);

    if (r_uint64_1 != uint64_1 || r_uint64_2 != uint64_2)
    {
        LOG_ERROR("Test uint64 failed")
        test_ok = false;
    }

    node_destroy(n_uint64_1);
    node_destroy(n_uint64_2);

    // Int64
    int64_t int64_1 = -1000000000064;
    node_t* n_int64_1 = node_create((void*)&int64_1, Int64, sizeof(int64_t));

    int64_t int64_2 = 2000000000000000064;
    node_t* n_int64_2 = node_create((void*)&int64_2, Int64, sizeof(int64_t));

    int64_t r_int64_1 = node_get_data_int64(n_int64_1);
    int64_t r_int64_2 = node_get_data_int64(n_int64_2);

    if (r_int64_1 != int64_1 || r_int64_2 != int64_2)
    {
        LOG_ERROR("Test int64 failed")
        test_ok = false;
    }

    node_destroy(n_int64_1);
    node_destroy(n_int64_2);

    // Float
    float float1 = 2.2f;
    node_t* n_float_1 = node_create((void*)&float1, Float, sizeof(float));

    float float2 = 2.000002f;
    node_t* n_float_2 = node_create((void*)&float2, Float, sizeof(float));

    float r_float_1 = node_get_data_float(n_float_1);
    float r_float_2 = node_get_data_float(n_float_2);

    if (r_float_1 != float1 || r_float_2 != float2)
    {
        LOG_ERROR("Test float failed")
        test_ok = false;
    }

    node_destroy(n_float_1);
    node_destroy(n_float_2);

    // Double
    double double1 = 2.2;
    node_t* n_double_1 = node_create((void*)&double1, Double, sizeof(double));

    double double2 = 2.000002;
    node_t* n_double_2 = node_create((void*)&double2, Double, sizeof(double));

    double r_double_1 = node_get_data_double(n_double_1);
    double r_double_2 = node_get_data_double(n_double_2);

    if (r_double_1 != double1 || r_double_2 != double2)
    {
        LOG_ERROR("Test double failed")
        test_ok = false;
    }

    node_destroy(n_double_1);
    node_destroy(n_double_2);

    // Array
    char* arr1 = calloc(10, 1);
    for (int i = 0; i < 10; i++)
    {
        arr1[i] = (char)10 + i;
    }
    node_t* n_arr_1 = node_create((void*)arr1, Array, 10);

    char* arr2 = calloc(200, 1);
    for (int i = 0; i < 10; i++)
    {
        arr2[i] = (char)i;
    }
    node_t* n_arr_2 = node_create((void*)arr2, Array, 200);

    uint64_t r_arr1_size = node_get_size(n_arr_1);
    uint64_t r_arr2_size = node_get_size(n_arr_2);
    if (r_arr1_size != 10 || r_arr2_size != 200)
    {
        LOG_ERROR("Test array failed")
        test_ok = false;
    }
    else
    {
        char* r_arr_1 = (char*)calloc(r_arr1_size, 1);
        char* r_arr_2 = (char*)calloc(r_arr2_size, 1);
        memset(r_arr_1, 0, 10);
        memset(r_arr_2, 0, 200);
        node_get_data_array(n_arr_1, r_arr_1);
        node_get_data_array(n_arr_2, r_arr_2);

        for (int i = 0; i < 10; i++)
        {
            if (r_arr_1[i] != arr1[i])
            {
                LOG_ERROR("Test array failed")
                test_ok = false;
            }
        }
        for (int i = 0; i < 200; i++)
        {
            if (r_arr_2[i] != arr2[i])
            {
                LOG_ERROR("Test array failed")
                test_ok = false;
            }
        }
        free(r_arr_1);
        free(r_arr_2);
        free(arr1);
        free(arr2);
    }

    node_destroy(n_arr_1);
    node_destroy(n_arr_2);

    if (test_ok == true)
    {
        LOG_INFO("All tests passed for node")
        return 0;
    }
    else
    {
        return -1;
    }

}

/*******************************************************************************
                                END OF FILE
*******************************************************************************/