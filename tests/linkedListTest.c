/*******************************************************************************
 *    File: linkedListTest.c
 *    Project: nachoLib
 *    Creation date: 13.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 21.03.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/
#include <stdbool.h>
#include <string.h>

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "Log.h"
#include "LinkedList.h"

/*******************************************************************************
                                    MAIN
*******************************************************************************/

int main (void)
{
    LOG_INFO("Test LinkedList running...")

    // Bool
    linked_list_t* list_bool = ll_create(Bool);
    bool bit1 = false;
    ll_insert_node(list_bool, 0, (void*)&bit1, sizeof(bool));
    bool bit2 = true;
    ll_insert_node(list_bool, 1, (void*)&bit2, sizeof(bool));

    bool rbool1 = ll_retrieve_bool(list_bool, 0);
    bool rbool2 = ll_retrieve_bool(list_bool, 1);

    if (rbool1 != bit1 || rbool2 != bit2)
    {
        LOG_ERROR("Test bool failed")
        return -1;
    }

    ll_destroy(list_bool);

    LOG_INFO("All tests passed for LinkedList")
    return 0;
}

/*******************************************************************************
                                END OF FILE
*******************************************************************************/