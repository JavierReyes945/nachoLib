/*******************************************************************************
 *    File: node.h
 *    Project: nachoLib
 *    Creation date: 13.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 13.04.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/**
 * @file Node.h
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef NODE_H
#define NODE_H

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <stdint.h>
#include <stdbool.h>

/*******************************************************************************
                                ENUMERATIONS
*******************************************************************************/

/**
 * @enum data_types_t
 * 
 * @brief Enum that holds the possible data types handled by the node
 * 
 */
typedef enum
{
    Bool,
    Char,
    UInt8,
    Int8,
    UInt16,
    Int16,
    UInt32,
    Int32,
    UInt64,
    Int64,
    Float,
    Double,
    Array
} data_types_t;

/*******************************************************************************
                                    STRUCTS
*******************************************************************************/

/**
 * @struct node_t
 * 
 * @brief Node Object structure
 * 
 */
typedef struct node_t
{
    void* _data;
    data_types_t _type;
    uint64_t _size;
    struct node_t* _next;
} node_t;

/*******************************************************************************
                        EXPORTED FUNCTION PROTOTYPES
*******************************************************************************/

node_t*  node_create          (void* data, data_types_t type, uint64_t size);
void     node_destroy         (node_t* node);
uint64_t node_get_size        (node_t* node);
bool     node_get_data_bool   (node_t* node);
char     node_get_data_char   (node_t* node);
uint8_t  node_get_data_uint8  (node_t* node);
int8_t   node_get_data_int8   (node_t* node);
uint16_t node_get_data_uint16 (node_t* node);
int16_t  node_get_data_int16  (node_t* node);
uint32_t node_get_data_uint32 (node_t* node);
int32_t  node_get_data_int32  (node_t* node);
uint64_t node_get_data_uint64 (node_t* node);
int64_t  node_get_data_int64  (node_t* node);
float    node_get_data_float  (node_t* node);
double   node_get_data_double (node_t* node);
void     node_get_data_array  (node_t* node, char* dst);

#endif /* NODE_H */

/*******************************************************************************
                                END OF FILE
*******************************************************************************/