/*******************************************************************************
 *    File: node.c
 *    Project: nachoLib
 *    Creation date: 13.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 13.04.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/**
 * @file Node.c
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "Node.h"
#include "Log.h"

/*******************************************************************************
                                    MACROS
*******************************************************************************/

/** 
 * @def Copy data between the origin pointer and the destination pointer
 *
 * @param src Pointer to the source data
 * @param dst Pointer to the data field in the node
 * @param type Datatype to cast the void pointer of src and dst
 */
#define NODE_COPY_DATA(src, dst, type)      *((type*)(dst)) = *((type*)(src))

/**
 * @def Copy data between the origin pointer and the destination pointer (to be called iterative)
 *
 * @param src Pointer to the source data
 * @param dst Pointer to the data field in the node
 * @param i index of the iteration
 */
#define NODE_COPY_DATA_P(src, dst, i)     ((char*)(dst))[i] = ((char*)(src))[i]

/*******************************************************************************
                        EXPORTED FUNCTION DEFINITIONS
*******************************************************************************/

/**
 * @brief Node constructor
 * 
 * @param data Pointer to the data to save in the node
 * @param type Enum value that defines which type of data is saved in the node
 * @return node_t* Pointer to the address where the node is allocated
 */
node_t* node_create(void *data, data_types_t type, uint64_t size)
{
    // Reserve memory for the node
    node_t *new_node_p = (node_t*)malloc(sizeof(node_t));
    LOG_DEBUG("node mem reserved: %" PRIu64, (uint64_t)sizeof(node_t))

    // Initialize object fields
    new_node_p->_type = type;
    switch (type)
    {
    case Bool:
        new_node_p->_size = sizeof(bool);
        break;
    case Char:
        new_node_p->_size = sizeof(char);
        break;
    case UInt8:
        new_node_p->_size = sizeof(uint8_t);
        break;
    case Int8:
        new_node_p->_size = sizeof(int8_t);
        break;
    case UInt16:
        new_node_p->_size = sizeof(uint16_t);
        break;
    case Int16:
        new_node_p->_size = sizeof(int16_t);
        break;
    case UInt32:
        new_node_p->_size = sizeof(uint32_t);
        break;
    case Int32:
        new_node_p->_size = sizeof(int32_t);
        break;
    case UInt64:
        new_node_p->_size = sizeof(uint64_t);
        break;
    case Int64:
        new_node_p->_size = sizeof(int64_t);
        break;
    case Float:
        new_node_p->_size = sizeof(float);
        break;
    case Double:
        new_node_p->_size = sizeof(double);
        break;
    case Array:
        new_node_p->_size = size;
        break;
    }
    new_node_p->_next = NULL;

    // Reserve memory for the data held in the node
    new_node_p->_data = malloc(new_node_p->_size);
    LOG_DEBUG("node mem data reserved: %" PRIu64, new_node_p->_size)

    // Copy the data to the reserved memory
    switch (new_node_p->_type)
    {
    case Bool:
        NODE_COPY_DATA(data, new_node_p->_data, bool);
        break;
    case Char:
        NODE_COPY_DATA(data, new_node_p->_data, char);
        break;
    case UInt8:
        NODE_COPY_DATA(data, new_node_p->_data, uint8_t);
        break;
    case Int8:
        NODE_COPY_DATA(data, new_node_p->_data, int8_t);
        break;
    case UInt16:
        NODE_COPY_DATA(data, new_node_p->_data, uint16_t);
        break;
    case Int16:
        NODE_COPY_DATA(data, new_node_p->_data, int16_t);
        break;
    case UInt32:
        NODE_COPY_DATA(data, new_node_p->_data, uint32_t);
        break;
    case Int32:
        NODE_COPY_DATA(data, new_node_p->_data, int32_t);
        break;
    case UInt64:
        NODE_COPY_DATA(data, new_node_p->_data, uint64_t);
        break;
    case Int64:
        NODE_COPY_DATA(data, new_node_p->_data, int64_t);
        break;
    case Float:
        NODE_COPY_DATA(data, new_node_p->_data, float);
        break;
    case Double:
        NODE_COPY_DATA(data, new_node_p->_data, double);
        break;
    case Array:
        for (uint64_t i = 0; i < new_node_p->_size; i++)
        {
            NODE_COPY_DATA_P(data, new_node_p->_data, i);
        }
        break;
    }

    // Address of the created node
    return new_node_p;
}

/**
 * @brief Node destructor
 * 
 * @param node pointer to the allocated node to destroy
 */
void node_destroy(node_t *node)
{
    // Release the memory of the data first
    free(node->_data);
    LOG_DEBUG("freed node data: %p", node->_data)

    // Then release the memory of the node itself
    free(node);
    LOG_DEBUG("freed node: %p", node)
}

/**
 * @brief get the size (in bytes) of the data held in the node
 * 
 * @param node Pointer to the node
 * @return uint64_t The number of bytes of the data held in the node, or 0 if node is empty
 */
uint64_t node_get_size(node_t* node)
{
    if (node->_data != NULL)
    {
        return node->_size;
    }
    else
    {
        return 0;
    }
}

/**
 * @brief get node data as bool
 *
 * @param node Pointer to the node
 * @return bool The value stored in data
 */
bool node_get_data_bool(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((bool*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as char
 * 
 * @param node Pointer to the node
 * @return char The value stored in data
 */
char node_get_data_char(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((char*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as uint8_t
 * 
 * @param node Pointer to the node
 * @return uint8_t The value stored in data
 */
uint8_t node_get_data_uint8(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((uint8_t*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as int8_t
 * 
 * @param node Pointer to the node
 * @return int8_t The value stored in data
 */
int8_t node_get_data_int8(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((int8_t*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as uint16_t
 * 
 * @param node Pointer to the node
 * @return uint16_t The value stored in data
 */
uint16_t node_get_data_uint16(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((uint16_t*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as uint16_t
 * 
 * @param node Pointer to the node
 * @return int16_t The value stored in data
 */
int16_t  node_get_data_int16(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((int16_t*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as uint32_t
 * 
 * @param node Pointer to the node
 * @return uint32_t The value stored in data
 */
uint32_t node_get_data_uint32(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((uint32_t*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as int32_t
 * 
 * @param node Pointer to the node
 * @return int32_t The value stored in data
 */
int32_t node_get_data_int32(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((int32_t*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as uint64_t
 * 
 * @param node Pointer to the node
 * @return uint64_t The value stored in data
 */
uint64_t node_get_data_uint64(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((uint64_t*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as int64_t
 * 
 * @param node Pointer to the node
 * @return int64_t The value stored in data
 */
int64_t node_get_data_int64(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((int64_t*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as float
 * 
 * @param node Pointer to the node
 * @return float The value stored in data
 */
float node_get_data_float(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((float*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as double
 * 
 * @param node Pointer to the node
 * @return double The value stored in data
 */
double node_get_data_double(node_t* node)
{
    if (node->_data != NULL)
    {
        return *((double*)(node->_data));
    }
    return false;
}

/**
 * @brief get node data as an array of char
 * 
 * @param node Pointer to the node
 * @param dst Pointer to a char array where the data shall be stored (it is
 *              expected to be large enough)
 */
void node_get_data_array(node_t* node, char* dst)
{
    if (node->_data != NULL && node->_size != 0)
    {
        for (uint64_t i = 0; i < node->_size; i++)
        {
            NODE_COPY_DATA_P(node->_data, dst, i);
        }
    }
}

/*******************************************************************************
                                END OF FILE
*******************************************************************************/