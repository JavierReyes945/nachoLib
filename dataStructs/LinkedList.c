/*******************************************************************************
 *    File: LinkedList.c
 *    Project: nachoLib
 *    Creation date: 13.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 13.04.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/**
 * @file LinkedList.c
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "LinkedList.h"
#include "Log.h"

/*******************************************************************************
                        PRIVATE FUNCTION PROTOTYPES
*******************************************************************************/

static node_t* ll_iterate (linked_list_t* link_list, uint64_t index);

/*******************************************************************************
                        PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/

/**
 * @brief Iterator function
 * 
 * @param link_list Pointer to the list to iterate
 * @param index 0-based index for the iteration
 * @return node_t* Pointer to the node under the given index
 */
static node_t* ll_iterate(linked_list_t* link_list, uint64_t index)
{
    // Shall not try if index is invalid
    if (index >= link_list->_length)
    {
        LOG_ERROR("Index out of bounds: %" PRIu64, index)
        return NULL;
    }

    // Start from first element of list
    node_t *cursor = link_list->_head;
    for (uint64_t i = 0; i < index; i++)
    {
        cursor = cursor->_next;
    }
    return cursor;
}

/*******************************************************************************
                        EXPORTED FUNCTION DEFINITIONS
*******************************************************************************/

/**
 * @brief LinkedList constructor
 * 
 * @param type Enum value that indicates the data type to store in the nodes
 * @return linked_list_t* Pointer to where the LinkedList ist allocated
 */
linked_list_t* ll_create(data_types_t type)
{
    // Reserve memory for the list object
    uint64_t list_mem = sizeof(linked_list_t);
    linked_list_t* new_list = (linked_list_t*)malloc(list_mem);
    LOG_DEBUG("linklist mem reserved: %" PRIu64, list_mem)

    // Initialize object fields
    new_list->_head = NULL;
    new_list->_length = 0;
    new_list->_type = type;

    // Pointer to the created object
    return new_list;
}

/**
 * @brief LinkedList destructor
 * 
 * @param link_list Pointer to the LinkedList to destroy
 */
void ll_destroy(linked_list_t* link_list)
{
    // Clear nodes first if list is not empty
    if (link_list->_length > 0)
    {
        LOG_WARNING("Destroying non-empty list: %" PRIu64, link_list->_length)
        while (link_list->_length > 0)
        {
            ll_remove_node(link_list, 0);
        }
    }

    // Release memory from list
    free(link_list);
}

/**
 * @brief Insert a new node under a given index (allocating memory for the new node)
 * 
 * @param link_list Pointer to the LinkedList where the new node will be inserted
 * @param index 0-based index where the new node will be inserted
 * @param data Pointer to the data that will be saved in the node
 * @param data_size Size in bytes of the data to hold
 */
void ll_insert_node(linked_list_t* link_list, uint64_t index, void* data, uint64_t data_size)
{
    // Call Node constructor
    LOG_DEBUG("Insert node started...")
    node_t *node_to_insert = node_create(data, link_list->_type, data_size);
    LOG_DEBUG("node created...")

    // Node insertion in list
    if (index == 0)
    {
        node_to_insert->_next = link_list->_head;
        link_list->_head = node_to_insert;
    }
    else
    {
        node_t *cursor = ll_iterate(link_list, index - 1);
        node_to_insert->_next = cursor->_next;
        cursor->_next = node_to_insert;
    }
    link_list->_length++;
}

/**
 * @brief Removes a node from the LinkedList in a given index
 * 
 * @param link_list Pointer to the LinkedList where the node shall be removed
 * @param index 0-based index where the node shall be removed
 */
void ll_remove_node(linked_list_t* link_list, uint64_t index)
{
    // Shall no try if index is invalid
    if (link_list->_length == 0)
    {
        LOG_ERROR("Attempting to remove node form empty list")
        return;
    }

    // Cursor node iteration
    node_t *node_to_remove;
    if (index == 0)
    {
        node_to_remove = link_list->_head;
        link_list->_head = node_to_remove->_next;
    }
    else
    {
        node_t *cursor = ll_iterate(link_list, index - 1);
        node_to_remove = cursor->_next;
        cursor->_next = node_to_remove->_next;
    }

    // Call Node destructor
    node_destroy(node_to_remove);
    link_list->_length--;
}

/**
 * @brief get the size (in bytes) of the data held in the node
 * 
 * @param link_list Pointer to the list where the node is located
 * @param index 0-based index where the data is located in the list
 * @return uint64_t The size (in bytes) of the data in that node, or 0 if node is empty
 */
uint64_t ll_get_node_size(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    if (cursor != NULL)
    {
        return cursor->_size;
    }
    else
    {
        return 0;
    }
}

/**
 * @brief Private function to return the value of the node as a bool
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return bool The value of the boolean stored in the desired node
 */
bool ll_retrieve_bool(linked_list_t *link_list, uint64_t index)
{
    node_t *cursor = ll_iterate(link_list, index);
    return node_get_data_bool(cursor);
}

/**
 * @brief Private function to return the value of the node as a char
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return char The value of the character stored in the desired node
 */
char ll_retrieve_char(linked_list_t *link_list, uint64_t index)
{
    node_t *cursor = ll_iterate(link_list, index);
    return node_get_data_char(cursor);
}

/**
 * @brief Private function to return the value of the node as an uint8_t
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return int The value of the integer stored in the desired node
 */
uint8_t ll_retrieve_uint8(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_uint8(cursor);
}

/**
 * @brief Private function to return the value of the node as an int8_t
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return int The value of the integer stored in the desired node
 */
int8_t ll_retrieve_int8(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_int8(cursor);
}

/**
 * @brief Private function to return the value of the node as an uint16_t
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return int The value of the integer stored in the desired node
 */
uint16_t ll_retrieve_uint16(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_uint16(cursor);
}

/**
 * @brief Private function to return the value of the node as an int16_t
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return int The value of the integer stored in the desired node
 */
int16_t ll_retrieve_int16(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_int16(cursor);
}

/**
 * @brief Private function to return the value of the node as an uint32_t
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return int The value of the integer stored in the desired node
 */
uint32_t ll_retrieve_uint32(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_uint32(cursor);
}

/**
 * @brief Private function to return the value of the node as an int32_t
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return int The value of the integer stored in the desired node
 */
int32_t ll_retrieve_int32(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_int32(cursor);
}

/**
 * @brief Private function to return the value of the node as an uint64_t
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return int The value of the integer stored in the desired node
 */
uint64_t ll_retrieve_uint64(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_uint64(cursor);
}

/**
 * @brief Private function to return the value of the node as an int64_t
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return int The value of the integer stored in the desired node
 */
int64_t ll_retrieve_int64(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_int64(cursor);
}

/**
 * @brief Private function to return the value of the node as a float
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return float The value of the float stored in the desired node
 */
float ll_retrieve_float(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_float(cursor);
}

/**
 * @brief Private function to return the value of the node as a double
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @return double The value of the double stored in the desired node
 */
double ll_retrieve_double(linked_list_t* link_list, uint64_t index)
{
    node_t* cursor = ll_iterate(link_list, index);
    return node_get_data_double(cursor);
}

/**
 * @brief Private function to return the value of the node as an array
 * 
 * @param link_list Pointer to the LinkedList where the data shall be retrieved
 * @param index 0-based index where the data shall be retrieved
 * @param dst Pointer to where the data shall be retrieved
 */
void ll_retrieve_array(linked_list_t* link_list, uint64_t index, char* dst)
{
    node_t* cursor = ll_iterate(link_list, index);
    node_get_data_array(cursor, dst);
}

/**
 * @brief Utility function to iterate and print the LinkedList contents
 * 
 * @param link_list Pointer to the LinkedList to print
 */
void ll_print(linked_list_t* link_list)
{
    // Base info about the list
    printf("List size:\t%" PRIu64 "\n", link_list->_length);

    // Data shall be typecasted according to type
    switch (link_list->_type)
    {
    case Bool:
        printf("List type:\tbool\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            bool value = ll_retrieve_bool(link_list, i);
            printf("\tItem:\t%" PRIu64 "\n\tValue:\t%s\n", i, (value)? "true":"false");
        }
        break;
    case Char:
        printf("List type:\tchar\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            char value = ll_retrieve_char(link_list, i);
            printf("\tItem:\t%" PRIu64 "\n\tValue:\t%c\n", i, value);
        }
        break;
    case UInt8:
        printf("List type:\tuint8_t\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            uint8_t value = ll_retrieve_uint8(link_list, i);
            printf("\tItem:\t%" PRIu64 "\n\tValue:\t%" PRIu8 " \n", i, value);
        }
        break;
    case Int8:
        printf("List type:\tint8_t\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            int8_t value = ll_retrieve_int8(link_list, i);
            printf("\tItem %" PRIu64 ":\t%" PRId8 "\n", i, value);
        }
        break;
    case UInt16:
        printf("List type:\tuint16_t\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            uint16_t value = ll_retrieve_uint16(link_list, i);
            printf("\tItem %" PRIu64 ":\t%" PRIu16 "\n", i, value);
        }
        break;
    case Int16:
        printf("List type:\tint16_t\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            int16_t value = ll_retrieve_int16(link_list, i);
            printf("\tItem %" PRIu64 ":\t%" PRId16 "\n", i, value);
        }
        break;
    case UInt32:
        printf("List type:\tuint32_t\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            uint32_t value = ll_retrieve_uint32(link_list, i);
            printf("\tItem %" PRIu64 ":\t%" PRIu32 "\n", i, value);
        }
        break;
    case Int32:
        printf("List type:\tint32_t\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            int32_t value = ll_retrieve_int32(link_list, i);
            printf("\tItem %" PRIu64 ":\t%" PRId32 "\n", i, value);
        }
        break;
    case UInt64:
        printf("List type:\tuint64_t\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            uint64_t value = ll_retrieve_uint64(link_list, i);
            printf("\tItem %" PRIu64 ":\t%" PRIu64 "\n", i, value);
        }
        break;
    case Int64:
        printf("List type:\tint64_t\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            int64_t value = ll_retrieve_int64(link_list, i);
            printf("\tItem %" PRIu64 ":\t%" PRId64 "\n", i, value);
        }
        break;
    case Float:
        printf("List type:\tfloat\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            float value = ll_retrieve_float(link_list, i);
            printf("\tItem %" PRIu64 ":\t%f\n", i, value);
        }
        break;
    case Double:
        printf("List type:\tdouble\n");
        for (uint64_t i = 0; i < link_list->_length; i++)
        {
            double value = ll_retrieve_double(link_list, i);
            printf("\tItem %" PRIu64 ":\t%f\n", i, value);
        }
        break;
    case Array:
        {
            printf("List type:\tArray\n");
            for (uint64_t i = 0; i < link_list->_length; i++)
            {
                uint64_t node_size = ll_get_node_size(link_list, i);
                char value[node_size];
                ll_retrieve_array(link_list, i, value);
                printf("\tItem %" PRIu64 ":\n", i);
                printf("\t\tData size: %" PRIu64 "\n\t\tValue: 0x", node_size);
                for (uint64_t byte = 0; byte < node_size; byte++)
                {
                    printf("%02" PRIx8, value[byte]);
                }
                printf("\n");
            }
            break;
        }
    }
}

/*******************************************************************************
                                END OF FILE
*******************************************************************************/