/*******************************************************************************
 *    File: Queue.c
 *    Project: nachoLib
 *    Creation date: 14.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 13.04.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/**
 * @file Queue.c
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <stdlib.h>
#include <inttypes.h>

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "Queue.h"
#include "Log.h"

/*******************************************************************************
                                    DEFINES
*******************************************************************************/

/** 
 * @def QUEUE_REMOVE_INDEX
 * 
 * @brief Elements will always be popped from the first index
 */
#define QUEUE_REMOVE_INDEX    0

/*******************************************************************************
                        EXPORTED FUNCTION DEFINITIONS
*******************************************************************************/

/**
 * @brief Queue contructor
 * 
 * @param type Enum value that indicates the data type to store in the nodes
 * @return queue_t* Pointer to where the Queue ist allocated
 */
queue_t* queue_create(queue_insert_t ins_type, data_types_t type)
{
    // Reserve memory for the queue
    queue_t* new_queue = (queue_t*)malloc(sizeof(queue_t));
    LOG_DEBUG("queue mem reserved: %" PRIu64, (uint64_t)sizeof(queue_t))

    // Call list constructor
    new_queue->_list = ll_create(type);

    // Initialize rest of fields
    new_queue->_ins_type = ins_type;

    // Pointer to created object
    return new_queue;
}

/**
 * @brief Queue destructor
 * 
 * @param queue Pointer to the Queue to destroy
 */
void queue_destroy(queue_t* queue)
{
    // First call internal list destructor
    ll_destroy(queue->_list);

    // Release queue memory
    free(queue);
}

/**
 * @brief Insert a new node in the Queue depending on queue type, allocating memory for the new node
 * 
 * @param queue Pointer to the Queue where the new node will be inserted
 * @param data Pointer to the data that will be saved in the node
 * @param data_size Size in bytes of data
 */
void queue_push(queue_t* queue, void* data, uint64_t data_size)
{
    // Define where to insert the node (first or last)
    uint64_t index_to_insert;
    if (queue->_ins_type == FIFO)
    {
        // Index is at the end (after last element)
        index_to_insert = queue->_list->_length;
    }
    else // LIFO
    {
        // Index is at the end (after last element)
        index_to_insert = 0;
    }

    // Call LinkedList insert API
    ll_insert_node(queue->_list, index_to_insert, data, data_size);
}

/**
 * @brief Removes a node from the Queue as boolean
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return bool Popped data value
 */
bool queue_pop_bool(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return false;
    }

    // Call LinkedList retrieve/remove API
    bool value = ll_retrieve_bool(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as char
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return char Popped data value
 */
char queue_pop_char(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return '\0';
    }

    // Call LinkedList retrieve/remove API
    char value = ll_retrieve_char(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as uint8_t
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return uint8_t Popped data value
 */
uint8_t queue_pop_uint8(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0;
    }

    // Call LinkedList retrieve/remove API
    uint8_t value = ll_retrieve_uint8(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as int8_t
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return int8_t Popped data value
 */
int8_t queue_pop_int8(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0;
    }

    // Call LinkedList retrieve/remove API
    int8_t value = ll_retrieve_int8(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as uint16_t
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return uint16_t Popped data value
 */
uint16_t queue_pop_uint16(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0;
    }

    // Call LinkedList retrieve/remove API
    uint16_t value = ll_retrieve_uint16(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as int16_t
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return int16_t Popped data value
 */
int16_t queue_pop_int16(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0;
    }

    // Call LinkedList retrieve/remove API
    int16_t value = ll_retrieve_int16(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as uint32_t
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return uint32_t Popped data value
 */
uint32_t queue_pop_uint32(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0;
    }

    // Call LinkedList retrieve/remove API
    uint32_t value = ll_retrieve_uint32(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as int32_t
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return int32_t Popped data value
 */
int32_t queue_pop_int32(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0;
    }

    // Call LinkedList retrieve/remove API
    int32_t value = ll_retrieve_int32(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as uint64_t
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return uint64_t Popped data value
 */
uint64_t queue_pop_uint64(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0;
    }

    // Call LinkedList retrieve/remove API
    uint64_t value = ll_retrieve_uint64(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as int64_t
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return int64_t Popped data value
 */
int64_t queue_pop_int64(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0;
    }

    // Call LinkedList retrieve/remove API
    int64_t value = ll_retrieve_int64(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as float
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return float Popped data value
 */
float queue_pop_float(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0.0f;
    }

    // Call LinkedList retrieve/remove API
    float value = ll_retrieve_float(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as double
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return double Popped data value
 */
double queue_pop_double(queue_t* queue)
{
    // Cannot pop from an empty Queue
    // TODO return?
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return 0.0;
    }

    // Call LinkedList retrieve/remove API
     double value = ll_retrieve_double(queue->_list, QUEUE_REMOVE_INDEX);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
    return value;
}

/**
 * @brief Removes a node from the Queue as an array of char*
 * 
 * @param queue Pointer to the Queue where the node shall be removed
 * @return char* Pointer to popped data array
 * 
 * @note The dst char array shall be large enough to hold the data (call queue_get_node_size before)
 */
void queue_pop_array(queue_t* queue, char* dst)
{
    // Cannot pop from an empty Queue
    if (queue->_list->_length == 0)
    {
        LOG_WARNING("Pop from an empty Queue")
        return;
    }

    // Call LinkedList retrieve/remove API
    ll_retrieve_array(queue->_list, QUEUE_REMOVE_INDEX, dst);
    ll_remove_node(queue->_list, QUEUE_REMOVE_INDEX);
}

/**
 * @brief Get the size of the data held at a given index
 * 
 * @param queue Pointer to the Queue to get the data from
 * @param index 0-based index where to get the data from
 * @return uint64_t Size (in bytes) of the array saved in the node
 */
uint64_t queue_get_array_size (queue_t* queue, uint64_t index)
{
    return ll_get_node_size(queue->_list, index);
}

/*******************************************************************************
                                END OF FILE
*******************************************************************************/