/*******************************************************************************
 *    File: Queue.h
 *    Project: nachoLib
 *    Creation date: 14.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 13.04.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/**
 * @file Queue.h
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef QUEUE_H
#define QUEUE_H

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <stdint.h>

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "LinkedList.h"

/*******************************************************************************
                                ENUMERATIONS
*******************************************************************************/

/**
 * @enum queue_insert_t
 * 
 * @brief Enum that holds the possible insertion types handled by the Queue
 * 
 */
typedef enum
{
    FIFO,   ///< First Input, Firts Output
    LIFO    ///< Last Input, Firts Output
} queue_insert_t;

/*******************************************************************************
                                    STRUCTS
*******************************************************************************/

/**
 * @struct queue_t
 * 
 * @brief Queue structure
 * 
 */
typedef struct queue_t
{
    linked_list_t* _list;
    queue_insert_t _ins_type;
} queue_t;

/*******************************************************************************
                        EXPORTED FUNCTION PROTOTYPES
*******************************************************************************/

queue_t* queue_create         (queue_insert_t ins_type, data_types_t type);
void     queue_destroy        (queue_t* queue);
void     queue_push           (queue_t* queue, void* data, uint64_t data_size);
bool     queue_pop_bool       (queue_t* queue);
char     queue_pop_char       (queue_t* queue);
uint8_t  queue_pop_uint8      (queue_t* queue);
int8_t   queue_pop_int8       (queue_t* queue);
uint16_t queue_pop_uint16     (queue_t* queue);
int16_t  queue_pop_int16      (queue_t* queue);
uint32_t queue_pop_uint32     (queue_t* queue);
int32_t  queue_pop_int32      (queue_t* queue);
uint64_t queue_pop_uint64     (queue_t* queue);
int64_t  queue_pop_int64      (queue_t* queue);
float    queue_pop_float      (queue_t* queue);
double   queue_pop_double     (queue_t* queue);
void     queue_pop_array      (queue_t* queue, char* dst);
uint64_t queue_get_array_size (queue_t* queue, uint64_t index);

#endif /* QUEUE_H */

/*******************************************************************************
                                END OF FILE
*******************************************************************************/