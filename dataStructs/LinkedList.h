/*******************************************************************************
 *    File: LinkedList.h
 *    Project: nachoLib
 *    Creation date: 13.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 13.04.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/**
 * @file LinkedList.h
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "Node.h"

/*******************************************************************************
                                    STRUCTS
*******************************************************************************/

/**
 * @struct linked_list_t
 * 
 * @brief LinkedList structure
 * 
 */
typedef struct linked_list_t
{
    node_t* _head;
    uint64_t _length;
    data_types_t _type;
} linked_list_t;

/*******************************************************************************
                        EXPORTED FUNCTION PROTOTYPES
*******************************************************************************/

linked_list_t* ll_create          (data_types_t type);
void           ll_destroy         (linked_list_t* link_list);
void           ll_insert_node     (linked_list_t* link_list, uint64_t index, void* data, uint64_t data_size);
void           ll_remove_node     (linked_list_t* link_list, uint64_t index);
uint64_t       ll_get_node_size   (linked_list_t* link_list, uint64_t index);
bool           ll_retrieve_bool   (linked_list_t* link_list, uint64_t index);
char           ll_retrieve_char   (linked_list_t* link_list, uint64_t index);
uint8_t        ll_retrieve_uint8  (linked_list_t* link_list, uint64_t index);
int8_t         ll_retrieve_int8   (linked_list_t* link_list, uint64_t index);
uint16_t       ll_retrieve_uint16 (linked_list_t* link_list, uint64_t index);
int16_t        ll_retrieve_int16  (linked_list_t* link_list, uint64_t index);
uint32_t       ll_retrieve_uint32 (linked_list_t* link_list, uint64_t index);
int32_t        ll_retrieve_int32  (linked_list_t* link_list, uint64_t index);
uint64_t       ll_retrieve_uint64 (linked_list_t* link_list, uint64_t index);
int64_t        ll_retrieve_int64  (linked_list_t* link_list, uint64_t index);
float          ll_retrieve_float  (linked_list_t* link_list, uint64_t index);
double         ll_retrieve_double (linked_list_t* link_list, uint64_t index);
void           ll_retrieve_array  (linked_list_t* link_list, uint64_t index, char* dst);
void           ll_print           (linked_list_t* link_list);

#endif /* LINKEDLIST_H */

/*******************************************************************************
                                END OF FILE
*******************************************************************************/