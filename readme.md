# Personal C Library

Goal is to keep as many common implementations as possible.

## Structure

- dataStructs
  - Node
  - LinkedList
  - Queue
  - BinaryTree
  - HashTable
  - Dictionary
- networking
  - SimpleSocket
  - Client
  - Server
  - HttpServer
- utils
  - Log

## Configuration

A header `nachoConfig.h`shall be placed in the include path of the project. A template can be found in `nachoConfigTemplate.h`.

## Build

- Library

```sh
cd build

make lib
```

## Tests

Every module has a corresponding test file in ` tests` folder. Each test is an independent program with a main function.

```c
make tests
./build/<test_name>
```

## Requirements

- C/C++ compiler toolchain (tested with GCC and Clang)
- `make` program
- Doxygen
- Plantuml local (path shall be configured in `Doxyfile`)
- Graphviz (For Doxygen internals)