/*******************************************************************************
 *    File: Server.c
 *    Project: nachoLib
 *    Creation date: 20.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 13.04.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/**
 * @file Server.c
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <inttypes.h>
#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#else
#include <arpa/inet.h>
#include <sys/poll.h>
#endif

/*******************************************************************************
                                USER HEADERS
*******************************************************************************/

#include "Server.h"
#include "Log.h"

/*******************************************************************************
                        PRIVATE FUNCTION PROTOTYPES
*******************************************************************************/

static char* get_ipadr_str(const struct sockaddr* sa, char* dst, size_t len);
static void* server_runner(void* arg);

/*******************************************************************************
                        PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/

/**
 * @brief Convert the server IP address into a string format
 * 
 * @param sa Pointer to the sockaddr structure, where the server address is located
 * @param dst Pointer to the char array, where the ip address string shall be written
 * @param len Size of the dst array
 * @return char* Pointer to the dst array if success, or NULL if error
 */
static char* get_ipadr_str(const struct sockaddr* sa, char* dst, size_t len)
{
    switch (sa->sa_family)
    {
    case AF_INET:
#ifdef _WIN32
        dst = inet_ntoa(((struct sockaddr_in*)sa)->sin_addr);
#else
        inet_ntop(AF_INET, &(((struct sockaddr_in*)sa)->sin_addr), dst, len);
#endif
        break;
    default:
        // TODO IPV6 implementation needed
        strncpy(dst, "Unknown AF", len);
        return NULL;
    }
    return dst;
}

/**
 * @brief Thread that will be executed when the server is launched for a specific socket
 * 
 * @param arg Pointer to the server structure to get the information about the connection or data
 * @return void* RESERVED
 */
static void* server_runner(void* arg)
{
#ifdef _WIN32
    FD_SET read_set;
#else
#endif
    char buffer[65536] = {0};
    const char* server_msg = "Server ACK\n";
    int bytes_to_send = strlen(server_msg) + 1;
    struct sockaddr_in client_adr;
    uint32_t client_adr_len = sizeof(client_adr);
#if _WIN32
#else
    struct pollfd pfd;
#endif

    // The object that launched the runner
    server_t* server = (server_t*)arg;

    LOG_INFO("Starting...")
    server->_is_running = true;

    // Server object will end this loop through server_stop
    while (server->_is_running == true)
    {
#ifdef _WIN32
        FD_ZERO(&read_set);
        FD_SET(server->_socket_fd, &read_set);
        int res_poll = select(0, &read_set, NULL, NULL, NULL);
#else
        pfd.fd = server->_socket_fd;
        pfd.events = POLLIN;

        int res_poll = poll(&pfd, 1, 10);
#endif
        if (res_poll == -1)
        {
            LOG_CRITICAL("Error by polling")
            return NULL;
        }
        else if (res_poll > 0)
        {
            // TCP Connection
            if (server->_type == STREAM)
            {
                // Accept client socket
#if _WIN32
                int client_socket = accept(server->_socket_fd, (struct sockaddr*)&client_adr, (int*)&client_adr_len);
#else
                int client_socket = accept(server->_socket_fd, (struct sockaddr*)&client_adr, &client_adr_len);
#endif
                if (client_socket == -1)
                {
                    LOG_CRITICAL("Failed to accept socket: %" PRId32, client_socket)
                }
                else
                {
                    LOG_DEBUG("Accepted socket %" PRId32 " from %s" PRId32, client_socket, inet_ntoa(client_adr.sin_addr));

                    // Read request from client
                    int recv_len = read(client_socket, buffer, sizeof(buffer));
                    LOG_INFO("Received %" PRId32 " bytes:\n\t%s", recv_len, buffer)

                    // Reply to client
                    write(client_socket, server_msg, bytes_to_send);
                    LOG_DEBUG("Request responded...")

                    // Clean buffer and close client socket
                    memset(buffer, 0, recv_len);
                    close(client_socket);
                }
            }
            // UDP Communication
            else if (server->_type == DATAGRAM)
            {
#if _WIN32
                int bytes_recv = recvfrom(server->_socket_fd, buffer, sizeof(buffer), 0, (struct sockaddr*)&client_adr, (int*)&client_adr_len);
#else
                int bytes_recv = recvfrom(server->_socket_fd, buffer, sizeof(buffer), 0, (struct sockaddr*)&client_adr, &client_adr_len);
#endif
                if (bytes_recv == 0)
                {
                    // Client disconnected
                    LOG_INFO("Client disconnected")
                }
                else if (bytes_recv == -1)
                {
                    // Error
                    LOG_CRITICAL("Error by recvfrom")
                }
                else
                {
                    // Handle received message
                    LOG_INFO("Message received from %s:\n\t%s", inet_ntoa(client_adr.sin_addr), buffer)
                    memset(buffer, 0, bytes_recv);

                    // Response
                    int bytes_sent = sendto(server->_socket_fd, server_msg, strlen(server_msg) + 1, 0, (struct sockaddr*)&client_adr, client_adr_len);
                    while (bytes_sent < bytes_to_send)
                    {
                        bytes_to_send -= bytes_sent;
                        bytes_sent = sendto(server->_socket_fd, &server_msg[bytes_sent], bytes_to_send, 0, (struct sockaddr*)&client_adr, client_adr_len);
                    }
                }
            }
        }
    }

    // Close Server socket
    close(server->_socket_fd);

    return NULL;
}

/*******************************************************************************
                        EXPORTED FUNCTION DEFINITIONS
*******************************************************************************/

/**
 * @brief Server object constructor
 * 
 * @param domain Enum value to define whether server is for IPV4 or IPV6
 * @param type Enum value to define the type of socket (TCP or UDP)
 * @param server_adr_str Pointer to the string, where the IP address to listen is located
 * @param port Defines the IP Port value
 * @param backlog How many transactions can the server accept
 * @return server_t* Pointer to the allocated server object
 */
server_t* server_create(socket_domain_t domain, socket_type_t type, const char* server_adr_str, int port, int backlog)
{
    LOG_DEBUG("Server constructor started...")

    // Allocate space for the server struct
    server_t* server = malloc(sizeof(server_t));
    LOG_DEBUG("Server mem reserved: %" PRIu64, (uint64_t)sizeof(server_t))

    // Struct fields initialization
    server->_domain = domain;
    server->_type = type;
    server->_port = port;
    server->_backlog = backlog;

    // Address to bind initialization
#if _WIN32
    server->_address.sin_family = domain;
#else
    server->_address.sin_family = (sa_family_t)domain;
#endif
    server->_address.sin_port = htons(port);
    server->_address.sin_addr.s_addr = inet_addr(server_adr_str);

    // Socket creation
    server->_socket_fd = socket(domain, type, 0);
#ifdef _WIN32
    if (server->_socket_fd == (int)INVALID_SOCKET)
#else
    if (server->_socket_fd < 0)
#endif
    {
        LOG_CRITICAL("Failed to create socket: %d", server->_socket_fd)
        return NULL;
    }
    LOG_DEBUG("Socket created: %d", server->_socket_fd)

    // Socket binding to the network address
    int res = bind(server->_socket_fd, (struct sockaddr*)&server->_address, sizeof(server->_address));
    if (res < 0)
    {
        LOG_CRITICAL("Failed to bind socket: %d", res)
        close(server->_socket_fd);
        exit(EXIT_FAILURE);
    }
    LOG_DEBUG("Socket binded...")

    // In case of UDP, no connections
    if (type == STREAM)
    {
        // Set listening on socket
        res = listen(server->_socket_fd, server->_backlog);
        if (res < 0)
        {
            LOG_CRITICAL("Failed to listen: %d", res)
            close(server->_socket_fd);
            exit(EXIT_FAILURE);
        }
        LOG_DEBUG("Socket listening...")
    }

    return server;
}

/**
 * @brief Server object destructor
 * 
 * @param server Pointer to the server object to destroy
 */
void server_destroy(server_t* server)
{
    // Close socket connetion
    if (server->_socket_fd > 0)
    {
        close(server->_socket_fd);
        LOG_DEBUG("Connection socket closed");
    }

    // Free Server memory
    free(server);
    LOG_DEBUG("freed node: %p", server)
}

/**
 * @brief Starts listening thread on the referenced server
 * 
 * @param server Pointer to the server object
 */
void server_launch(server_t* server)
{
    char ip_add[16];
    get_ipadr_str((struct sockaddr*)(&server->_address), ip_add, sizeof(ip_add));
    LOG_INFO("launching server on %s:%" PRId32 " ...", ip_add, server->_port)

    int res = pthread_create(&server->_runner_id, NULL, server_runner, (void*)server);
    if (res != 0)
    {
        LOG_CRITICAL("Thread could not be created")
    }

    LOG_INFO("Server launched")
}

/**
 * @brief Stops the listening thread (later needs to be destroyed)
 * 
 * @param server Pointer to the server object
 */
void server_shutdown(server_t* server)
{
    LOG_DEBUG("Shuting down server...")
    if (server->_is_running == true)
    {
        server->_is_running = false;
    }
    pthread_join(server->_runner_id, NULL);
    LOG_DEBUG("Server shutdown");
}

/*******************************************************************************
                                END OF FILE
*******************************************************************************/