/*******************************************************************************
 *    File: Server.h
 *    Project: nachoLib
 *    Creation date: 20.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 13.04.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

#ifndef SERVER_H
#define SERVER_H

/**
 * @file Server.h
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <sys/types.h>
#ifdef _WIN32
#include <winsock2.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#endif
#include <stdbool.h>
#include <pthread.h>

/*******************************************************************************
                                ENUMERATIONS
*******************************************************************************/

/**
 * @enum socket_domain_t
 * 
 * @brief Enum encapsulation for the types in socket.h
 * 
 */
typedef enum
{
    UNIX = AF_UNIX,
    IPV4 = AF_INET,
    IPV6 = AF_INET6
} socket_domain_t;

/**
 * @enum socket_type_t
 * 
 * @brief Enum encapsulation for te types in socket.h
 * 
 */
typedef enum
{
    STREAM = SOCK_STREAM,
    DATAGRAM = SOCK_DGRAM,
    RAW = SOCK_RAW
} socket_type_t;

/*******************************************************************************
                                    STRUCTS
*******************************************************************************/

/**
 * @struct server_t
 * 
 * @brief Server Object structure
 * 
 */
typedef struct server_t
{
    // Socket parameters
    socket_domain_t _domain;
    socket_type_t _type;

    // Bind parameters
    int _port;
    int _backlog;

    // Bind address
    struct sockaddr_in _address;

    // Socket file descriptor
    int _socket_fd;

    // Status of the launch
    bool _is_running;
    pthread_t _runner_id;
} server_t;

/*******************************************************************************
                        EXPORTED FUNCTION PROTOTYPES
*******************************************************************************/

server_t* server_create   (socket_domain_t domain, socket_type_t type, const char* server_adr_str, int port, int backlog);
void      server_destroy  (server_t* server);
void      server_launch   (server_t* server);
void      server_shutdown (server_t* server);

#endif /* SERVER_H */

/*******************************************************************************
                                END OF FILE
*******************************************************************************/