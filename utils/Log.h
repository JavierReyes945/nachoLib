/*******************************************************************************
 *    File: Log.h
 *    Project: nachoLib
 *    Creation date: 13.03.2021
 *    Author: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 *    Date modified: 20.03.2021
 *    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
 * 
 * MIT License
 * 
 * Copyright (c) 2021
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*******************************************************************************/

/**
 * @file Log.h
 * @author Javier Reyes (javier.reyes.g@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef LOG_H
#define LOG_H

/*******************************************************************************
                                SYSTEM HEADERS
*******************************************************************************/

#include <stdio.h>

/*******************************************************************************
                                    DEFINES
*******************************************************************************/

/**
 * @def LOG_LEVEL
 * 
 * @brief Defines which level to use for debug messages in stdout
 *          0: No messages
 *          1: Critical
 *          2: Error
 *          3: Warning
 *          4: Info
 *          5: Debug
 */
#define LOG_LEVEL   5

/**
 * @def DEBUG(text, ...)
 * @brief Simple macro to log information onto the stdout with format
 * @param text [string] The message to print (can include formatters)
 * @param ... Arguments to the string formatters (if any)
 */
#if LOG_LEVEL == 0
#define LOG_DEBUG(text, ...)        {}
#define LOG_INFO(text, ...)         {}
#define LOG_WARNING(text, ...)      {}
#define LOG_ERROR(text, ...)        {}
#define LOG_CRITICAL(text, ...)     {}
#elif LOG_LEVEL == 1
#define LOG_DEBUG(text, ...)        {}
#define LOG_INFO(text, ...)         {}
#define LOG_WARNING(text, ...)      {}
#define LOG_ERROR(text, ...)        {}
#define LOG_CRITICAL(text, ...)     {printf("[CRITICAL][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#elif LOG_LEVEL == 2
#define LOG_DEBUG(text, ...)        {}
#define LOG_INFO(text, ...)         {}
#define LOG_WARNING(text, ...)      {}
#define LOG_ERROR(text, ...)        {printf("[ERROR][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_CRITICAL(text, ...)     {printf("[CRITICAL][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#elif LOG_LEVEL == 3
#define LOG_DEBUG(text, ...)        {}
#define LOG_INFO(text, ...)         {}
#define LOG_WARNING(text, ...)      {printf("[WARNING][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_ERROR(text, ...)        {printf("[ERROR][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_CRITICAL(text, ...)     {printf("[CRITICAL][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#elif LOG_LEVEL == 4
#define LOG_DEBUG(text, ...)        {}
#define LOG_INFO(text, ...)         {printf("[INFO][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_WARNING(text, ...)      {printf("[WARNING][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_ERROR(text, ...)        {printf("[ERROR][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_CRITICAL(text, ...)     {printf("[CRITICAL][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#elif LOG_LEVEL == 5
#define LOG_DEBUG(text, ...)        {printf("[DEBUG][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_INFO(text, ...)         {printf("[INFO][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_WARNING(text, ...)      {printf("[WARNING][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_ERROR(text, ...)        {printf("[ERROR][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#define LOG_CRITICAL(text, ...)     {printf("[CRITICAL][%s %d] "text"\n", __FILE__, __LINE__, ##__VA_ARGS__);}
#endif

#endif /* LOG_H */

/*******************************************************************************
                                END OF FILE
*******************************************************************************/