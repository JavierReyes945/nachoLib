################################################################################
#    File: Makefile
#    Project: nachoLib
#    Creation date: 14.03.2021
#    Author: Javier Reyes (javier.reyes.g@gmail.com)
# 
#    Date modified: 21.03.2021
#    Modified by: Javier Reyes (javier.reyes.g@gmail.com)
# 
# MIT License
# 
# Copyright (c) 2021
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
################################################################################

################################################################################
#                               COMPILER SETTINGS
################################################################################

CC := gcc
CFLAGS := -std=c11 -Wall -Wextra -O0 -g3

################################################################################
#                                   PATHS
################################################################################

BUILD_DIR := build

INC_PATHS := \
utils \
dataStructs \
networking

INCS := $(addprefix -I, $(INC_PATHS))

ifeq ($(OS), Windows_NT)
SOURCES := \
dataStructs/Node.c \
dataStructs/LinkedList.c \
dataStructs/Queue.c \
networking/Server.c
TEST_SOURCES := \
tests/nodeTest.c \
tests/linkedListTest.c \
tests/queueTest.c \
tests/serverTest.c
else
SOURCES := $(shell find . -not -path '*tests*' -name '*.c')
TEST_SOURCES := $(shell find . -path '*tests*' -name '*Test.c')
endif

SOURCE_PATHS := $(dir $(SOURCES))
TEST_PATHS := $(dir $(TEST_SOURCES))

VPATH := $(SOURCE_PATHS) $(INC_PATHS) $(TEST_PATHS) $(BUILD_DIR)

OBJECTS := $(addprefix $(BUILD_DIR)/, $(patsubst %.c, %.o, $(notdir $(SOURCES))))
TESTS := $(addprefix $(BUILD_DIR)/, $(basename $(notdir $(TEST_SOURCES))))

################################################################################
#                              RULE DEFINITIONS
################################################################################

.PHONY: clean all docs lib tests

all: lib tests

################################################################################
#                                 LIBRARY
################################################################################

NAME := nacho
LIB_NAME := lib$(NAME).a

lib: $(BUILD_DIR)/$(LIB_NAME)

$(BUILD_DIR)/$(LIB_NAME): $(OBJECTS)
	ar vrcs $@ $^

################################################################################
#                                 OBJECTS
################################################################################

$(BUILD_DIR)/Node.o: Node.c Node.h Log.h
	$(CC) -c $(CFLAGS) $(INCS) -o $@ $<

$(BUILD_DIR)/LinkedList.o: LinkedList.c LinkedList.h Node.h Log.h
	$(CC) -c $(CFLAGS) $(INCS) -o $@ $<

$(BUILD_DIR)/Queue.o: Queue.c Queue.h LinkedList.h Node.h Log.h
	$(CC) -c $(CFLAGS) $(INCS) -o $@ $<

$(BUILD_DIR)/Server.o: Server.c Server.h Log.h
	$(CC) -c $(CFLAGS) $(INCS) -o $@ $<

################################################################################
#                                 TESTS
################################################################################

tests: $(TESTS) $(BUILD_DIR)/$(LIB_NAME)
	@echo 'Tests started...'
#	./$(BUILD_DIR)/nodeTest
#	./$(BUILD_DIR)/linkedListTest
#	./$(BUILD_DIR)/queueTest
#	./$(BUILD_DIR)/serverTest
	@echo 'Tests completed!'

$(BUILD_DIR)/nodeTest: nodeTest.c $(BUILD_DIR)/$(LIB_NAME)
	$(CC) $(CFLAGS) $(INCS) -o $@ $< -L$(BUILD_DIR) -l$(NAME)

$(BUILD_DIR)/linkedListTest: linkedListTest.c $(BUILD_DIR)/$(LIB_NAME)
	$(CC) $(CFLAGS) $(INCS) -o $@ $< -L$(BUILD_DIR) -l$(NAME)

$(BUILD_DIR)/queueTest: queueTest.c $(BUILD_DIR)/$(LIB_NAME)
	$(CC) $(CFLAGS) $(INCS) -o $@ $< -L$(BUILD_DIR) -l$(NAME)

$(BUILD_DIR)/serverTest: serverTest.c $(BUILD_DIR)/$(LIB_NAME)
ifeq ($(OS), Windows_NT)
	$(CC) $(CFLAGS) $(INCS) -o $@ $< -L$(BUILD_DIR) -l$(NAME) -lpthread -lregex -lws2_32
else
	$(CC) $(CFLAGS) $(INCS) -o $@ $< -L$(BUILD_DIR) -l$(NAME) -lpthread
endif

################################################################################
#                               MISCELANEOUS
################################################################################

clean:
	rm -fv $(BUILD_DIR)/*.o $(BUILD_DIR)/*.a $(BUILD_DIR)/*Test*

docs:
	doxygen Doxyfile

valgrind: $(TESTS)
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose ./$(BUILD_DIR)/serverTest

################################################################################
#                               END OF FILE
################################################################################